package edu.tracktoreast.customer;

import edu.tracktoreast.clients.fraud.FraudCheckResponse;
import edu.tracktoreast.clients.fraud.FraudClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class CustomerService {

	private final CustomerRepository customerRepository;
	private final RestTemplate restTemplate;
	private final FraudClient fraudClient;

	public Long registerCustomer(CustomerRegistrationRequest request) {
		Customer customer = Customer.builder()
				.firstName(request.firstName())
				.lastName(request.lastName())
				.email(request.email())
				.build();
		// todo: validate
		customerRepository.saveAndFlush(customer);

		FraudCheckResponse fraudCheckResponse = fraudClient.isFraudster(customer.getId());

		if (fraudCheckResponse != null && fraudCheckResponse.isFraudster()) {
			throw new IllegalStateException("fraudster");
		}

		return customer.getId();

		// todo: send notification
	}
}
