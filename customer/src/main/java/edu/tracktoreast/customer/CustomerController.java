package edu.tracktoreast.customer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("api/v1/customers")
public class CustomerController {

	private final CustomerService customerService;


	@PostMapping
	public Long registerCustomer(@RequestBody CustomerRegistrationRequest customerRegistrationRequest) {
		log.info("new customer registration {}", customerRegistrationRequest);
		return customerService.registerCustomer(customerRegistrationRequest);
	}
}
