package edu.tracktoreast.clients.fraud;

public record FraudCheckResponse(Boolean isFraudster) {
}
