package edu.tracktoreast.fraud;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Service
public class FraudCheckService {

	private final FraudCheckHistoryRepository fraudCheckHistoryRepository;

	public boolean isFraudulentCustomer(Long customerId) {
		// checks if customer is fraudulent
		fraudCheckHistoryRepository.save(
				FraudCheckHistory.builder()
						.customerId(customerId)
						.isFraudster(false)
						.createdAt(LocalDateTime.now())
						.build()
		);
		return false;
	}

}
