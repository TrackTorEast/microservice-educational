package edu.tracktoreast.fraud;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class FraudCheckHistory {

	@Id
	// sequence name is fraud_id_sequence
	@SequenceGenerator(
			name = "fraud_id_sequence",
			sequenceName = "fraud_id_sequence"
	)
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "fraud_id_sequence"

	)
	private Long id;

	private Long customerId;

	private Boolean isFraudster;

	private LocalDateTime createdAt;
}
